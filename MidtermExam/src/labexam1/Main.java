package labexam1;

import java.util.Collections;
import java.util.List;

import store.LineItem;
import store.Store;

/**
 * Main class, entry point of the application.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.18 EXAM
 */
public class Main {

	/**
	 * Entry point of the application.
	 * 
	 * @param args
	 *            - not used
	 */
	public static void main(String[] args) {
		// Get summarized list from SalesAnalyzer.
		List<LineItem> summarizedSalesList = SalesAnalyzer.summarize(Store
				.getInstance().iterator());

		// Sort the list by productIds and display.
		Collections.sort(summarizedSalesList);
		System.out
				.println("+-------------------------------------------------------------------------------------+");
		System.out
				.println("| All sales sorted by Product ID                                                      |");
		SalesAnalyzer.printReport(summarizedSalesList);
		System.out
				.println("=======================================================================================");

		// Sort the list by item quantities and display.
		Collections.sort(summarizedSalesList, new QuantityComparator());
		System.out
				.println("+-------------------------------------------------------------------------------------+");
		System.out
				.println("| All sales sorted by Quantities                                                      |");
		SalesAnalyzer.printReport(summarizedSalesList);
		System.out
				.println("=======================================================================================");

		// Display grand total sales amount.
		double grandTotal = 0.0;
		for (LineItem li : summarizedSalesList)
			grandTotal += li.getTotal();
		System.out
				.println("+---------------------------------------------------------------------+---------------+");
		System.out
				.printf("| GRAND TOTAL                                                         | %,13.2f |\n",
						grandTotal);
		System.out
				.println("+---------------------------------------------------------------------+---------------+");
		System.out.println();
	}
}
