package labexam1;

import java.util.Comparator;
import store.LineItem;

/**
 * A Comparator for LineItem(s) that will sort them by their quantities.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.18 EXAM
 */
public class QuantityComparator implements Comparator<LineItem> {

	/**
	 * Compare two {@link store.LineItem}s by their quantities. If their
	 * quantities are equal, the item with lower productId will comes first.
	 * 
	 * @param item1
	 *            - the first item to be compared
	 * @param item2
	 *            - the second item to be compared
	 * @return Comparison result.
	 */
	@Override
	public int compare(LineItem item1, LineItem item2) {
		if (item1.getQuantity() == item2.getQuantity()) {
			return (int) (item1.getProductId() - item2.getProductId());
		}
		return item2.getQuantity() - item1.getQuantity();
	}
}