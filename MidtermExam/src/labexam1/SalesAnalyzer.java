package labexam1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import store.LineItem;
import store.Sale;

/**
 * Utility class for {@link store.Store}, provides many useful tools, such as
 * summarizing sales or printing sales report.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.18 EXAM
 */
public class SalesAnalyzer {

	/**
	 * Summarize {@link store.Sale}s into a List of {@link store.LineItem}s that
	 * are sold.
	 * 
	 * @param iterator
	 *            - an iterator from {@link store.Store#iterator()}
	 * @return A List of {@link store.LineItem}s that are sold.
	 */
	public static List<LineItem> summarize(Iterator<Sale> iterator) {
		List<LineItem> summarizedItemsList = new ArrayList<LineItem>();
		while (iterator.hasNext()) {

			// Iterate over every items in each sale.
			for (LineItem sale_listItem : iterator.next().getItems()) {

				// Check if the item already exists in summarizedItemsList by
				// comparing
				// productIds, the final result won't be null if it already
				// exists.
				LineItem cached_summarizedListItem = null;

				// Iterate over every item in summarizedItemsList to find
				// matching productId.
				for (LineItem summarizedListItem : summarizedItemsList) {
					if (sale_listItem.getProductId() == summarizedListItem
							.getProductId()) {
						cached_summarizedListItem = summarizedListItem;
						break;
					}
				}

				if (cached_summarizedListItem != null) {

					// If the item already exist in summarizedItemsList, update
					// its quantity.
					cached_summarizedListItem
							.setQuantity(cached_summarizedListItem
									.getQuantity()
									+ sale_listItem.getQuantity());

				} else {

					// But if the item does not exist, add it to
					// summarizedItemsList.
					summarizedItemsList.add(sale_listItem);

				}
			}
		}
		return summarizedItemsList;
	}

	/**
	 * Print out a report of items in the List of {@link store.LineItem}s to the
	 * console.
	 * 
	 * @param itemsList
	 *            - the list of {@link store.LineItem} to be printed
	 */
	public static void printReport(List<LineItem> itemsList) {
		System.out
				.println("+------------+------------------------------+----------+--------------+---------------+");
		System.out
				.println("| Product ID |         Description          | Quantity |  Price/Unit  |     Price     |");
		System.out
				.println("+------------+------------------------------+----------+--------------+---------------+");
		for (LineItem li : itemsList) {
			System.out.printf("| %10d | %-28s | %8d | %,12.2f | %,13.2f |\n",
					li.getProductId(), li.getDescription(), li.getQuantity(),
					li.getUnitPrice(), li.getTotal());
		}
		System.out
				.println("+------------+------------------------------+----------+--------------+---------------+");
	}
}